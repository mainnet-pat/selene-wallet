import COLOURS from "@selene-wallet/common/dist/design/colours";
import SPACING from "@selene-wallet/common/dist/design/spacing";

const styles = {
  header: {
    width: "100%",
    height: SPACING.fifty,
    backgroundColor: COLOURS.black,
  },
};

export default styles;
