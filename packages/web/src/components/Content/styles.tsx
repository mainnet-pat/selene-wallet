import COLOURS from "@selene-wallet/common/dist/design/colours";
import SPACING from "@selene-wallet/common/dist/design/spacing";

const styles = {
  content: {
    width: "100%",
    height: SPACING.fifty * 4,
    backgroundColor: COLOURS.veryLightGrey,
  },
};

export default styles;
