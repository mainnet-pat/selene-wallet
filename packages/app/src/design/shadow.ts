import COLOURS from "@selene-wallet/common/design/colours";

const SHADOW = {
  // iOS drop shadow
  shadowColor: COLOURS.black,
  shadowOffset: { width: -2, height: 4 },
  shadowOpacity: 0.2,
  shadowRadius: 3,
  // Android drop shadow
  elevation: 3,
};

export default SHADOW;
