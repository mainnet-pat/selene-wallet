import COLOURS from "@selene-wallet/common/design/colours";
import SPACING from "@selene-wallet/common/design/spacing";

const styles = {
  scrollView: {
    backgroundColor: COLOURS.black,
  },
  container: {
    backgroundColor: COLOURS.white,
    padding: SPACING.fifteen,
    // justifyContent: "center",
    // alignItems: "center",
    borderRadius: SPACING.borderRadius,
  },
};

export default styles;
