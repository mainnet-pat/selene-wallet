import COLOURS from "@selene-wallet/common/design/colours";
import SPACING from "@selene-wallet/common/design/spacing";

const styles = {
  container: {
    backgroundColor: COLOURS.black,
    paddingTop: SPACING.fifteen,
    paddingLeft: SPACING.twentyFive,
    paddingRight: SPACING.twentyFive,
    paddingBottom: SPACING.twentyFive,
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
  },
};

export default styles;
