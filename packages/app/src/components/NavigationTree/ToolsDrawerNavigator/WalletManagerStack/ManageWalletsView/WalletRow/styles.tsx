import SPACING from "@selene-wallet/common/design/spacing";

const styles = {
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  padding: {
    flex: 1,
    paddingLeft: SPACING.ten,
    paddingRight: SPACING.ten,
  },
  fixedWidth: { width: 100 },
};

export default styles;
