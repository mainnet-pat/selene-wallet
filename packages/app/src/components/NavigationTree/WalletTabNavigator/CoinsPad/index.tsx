import React from "react";
import { View, Text } from "react-native";
import { useSelector } from "react-redux";
import styles from "./styles";
import { ReduxState } from "@selene-wallet/common/dist/types";
import {
  selectActiveWallet,
} from "@selene-wallet/app/src/redux/selectors";
import Button from "../../../atoms/Button";
import store from "../../../../redux/store";
import { updateWalletMaxAddressIndex } from "../../../../redux/reducers/walletManagerReducer";

const ReceivePad = ({ }) => {

  const wallet = useSelector((state: ReduxState) => selectActiveWallet(state));

  const addAddresses = () => {
    store.dispatch(
      updateWalletMaxAddressIndex({
        name: wallet.name,
        maxAddressIndex: wallet.maxAddressIndex + 10,
      })
    );
  }

  return (
    <View>
      <View>
          <Text>Tracked addresses: {wallet.maxAddressIndex}</Text>
          <Button
            onPress={addAddresses}
            variant={"primary"}
          >
            Scan 10 more addresses
          </Button>
          {wallet.coins && wallet.coins?.map(val =>
            <View style={styles.coinView as any} key={`${val.txid}:${val.vout}`}>
              <Text>Coin index: {val.addressIndex}</Text>
              <Text>Address: {val.address.split(":")[1].slice(0,30)}...</Text>
              <Text>Value: {val.satoshis}</Text>
              <Text>Outpoint: {`${val.txid.slice(0,30)}...:${val.vout}`}</Text>
            </View>
          )}
      </View>
    </View>
  );
};

export default ReceivePad;
