import COLOURS from "@selene-wallet/common/design/colours";
import SPACING from "@selene-wallet/common/design/spacing";

const styles = {
  coinView: {
    height: 80,
    borderColor: COLOURS.lightGrey,
    borderWidth: 5,
    justifyContent: "center",
    alignItems: "left",
    width: "97%",
    padding: SPACING.five,
    margin: SPACING.five,
    borderRadius: SPACING.borderRadius,
  }
};

export default styles;
