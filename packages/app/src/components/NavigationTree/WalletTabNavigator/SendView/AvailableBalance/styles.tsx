import COLOURS from "@selene-wallet/common/design/colours";
import SPACING from "@selene-wallet/common/design/spacing";

const styles = {
  container: {
    backgroundColor: COLOURS.black,
    paddingTop: SPACING.ten,
  },
  logo: {
    width: 75,
    height: 75,
    marginBottom: SPACING.ten,
  },
  primaryTitlesWrapper: {
    marginBottom: SPACING.five,
  },
};

export default styles;
