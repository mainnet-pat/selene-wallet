import SPACING from "@selene-wallet/common/design/spacing";

const styles = {
  entryColumn: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: "100%",
    width: "100%",
    margin: SPACING.five,
    paddingRight: SPACING.twentyFive,
    paddingLeft: SPACING.twentyFive,
  },
};

export default styles;
