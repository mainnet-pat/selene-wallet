const TRANSACTION_PAD_ERRORS = {
  INSUFFICIENT_BALANCE: "Insufficient balance.",
  ALREADY_USED_DECIMAL: "Already used decimal point.",
  MAXIMUM_DECIMAL_PLACES: "Maximum decimal places.",
};

export default TRANSACTION_PAD_ERRORS;
