import { emit } from "react-native-react-bridge/lib/web";
import { RESPONSE_MESSAGE_TYPES } from "@selene-wallet/app/src/utils/bridgeMessages";

export const sendCoins = async (WalletObject, message) => {
  const walletSendCoins = await WalletObject.fromSeed(
    message?.data?.mnemonic,
    message?.data?.derivationPath
  );

  try {
    let amountAvailable = 0;
    const suitableCoins = [];

    // find suitable coins to be spent and get their total value
    for (const coin of message.data?.coins) {
      if (coin.token) {
        continue;
      }

      suitableCoins.push(coin);
      amountAvailable += coin.satoshis;
      if (amountAvailable > message?.data?.satsToSend) {
        break;
      }
    }

    if (message?.data?.satsToSend > amountAvailable) {
      throw new Error("Not enough funds");
    }

    // initialize libauth template and compiler
    const template = libauth.importAuthenticationTemplate(
      libauth.authenticationTemplateP2pkhNonHd
    );
    const compiler = await libauth.authenticationTemplateToCompilerBCH(template);

    // utility function to get the locking bytecode from cashaddr
    const cashAddressToLockingBytecode = (cashaddr) => {
      const outputLockingBytecode = libauth.cashAddressToLockingBytecode(cashaddr);
      if (typeof outputLockingBytecode === "string")
        throw new Error(outputLockingBytecode);
      return outputLockingBytecode.bytecode;
    }

    // reusable function to build transaction for fee estimation and final transaction given the fee
    const buildTransaction = async (fee) => {
      const changeAmount = amountAvailable - message?.data?.satsToSend - fee;
      const outputs = [{
        lockingBytecode: cashAddressToLockingBytecode(message?.data?.recipientCashAddr),
        valueSatoshis: BigInt(message?.data?.satsToSend),
      }];

      // discard dust change
      if (changeAmount >= 546) {
        outputs.push({
          lockingBytecode: cashAddressToLockingBytecode(message?.data?.changeAddress),
          valueSatoshis: BigInt(changeAmount),
        });
      }

      const inputs = await Promise.all(suitableCoins.map(async (val) => ({
        outpointIndex: val.vout,
        outpointTransactionHash: libauth.hexToBin(val.txid),
        sequenceNumber: 0,
        unlockingBytecode: {
          compiler,
          data: {
            keys: { privateKeys: { key: (await WalletObject.fromSeed(
              message?.data?.mnemonic,
              `m/44'/0'/0'/0/${val.addressIndex}`
            )).privateKey } },
          },
          valueSatoshis: BigInt(val.satoshis),
          script: "unlock",
        },
      })));
      console.log(inputs, outputs)

      const result = libauth.generateTransaction({
        inputs: inputs,
        outputs: outputs,
        locktime: 0,
        version: 2,
      });

      if (!result.success) {
        throw "Error building transaction";
      }

      return libauth.encodeTransaction(result.transaction);
    }

    // smallest fee - 220 bytes for single input two outputs (recipient and change)
    const estimatedTx = await buildTransaction(220);
    const fee = estimatedTx.length;

    // rebuild transaction with the estimated fee
    const finalTx = await buildTransaction(fee);

    // get a transient wallet and send the built transaction
    const tempWallet = await WalletObject.newRandom();
    await tempWallet.submitTransaction(finalTx, true);

    // Note: Monitoring .send() response for send confirmation
    // is unreliable and buggy
    // Instead, successful sends are detected by balance changes
  } catch (sendError) {
    console.trace("!!!!!!!!");

    emit({
      type: RESPONSE_MESSAGE_TYPES.SEND_COINS_RESPONSE_FAIL,
      data: {
        text: sendError?.message || "",
      },
    });
  }
};

export const getWalletHistory = async (WalletObject, message) => {
  console.log("calling getWalletHistory");
  const maxIndex = message?.data?.maxAddressIndex || 0;
  const result = await Promise.all([...Array(maxIndex + 1).keys()].map(async (index) => {
    const wallet = await WalletObject.fromSeed(
      message?.data?.mnemonic,
      `m/44'/0'/0'/0/${index}`
    );

    return (await wallet.getHistory("sat", 0, 100)).transactions;
  }));

  const transactionHistory = result.flat(1);
  emit({
    type: RESPONSE_MESSAGE_TYPES.GET_WALLET_HISTORY_RESPONSE,
    data: {
      name: message?.data?.name,
      transactionHistory: {transactions: transactionHistory},
    },
  });

};
