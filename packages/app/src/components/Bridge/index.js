import React, { useState } from "react";
import {
  webViewRender,
  emit,
  useNativeMessage,
} from "react-native-react-bridge/lib/web";
import {
  BRIDGE_MESSAGE_TYPES,
  RESPONSE_MESSAGE_TYPES,
} from "@selene-wallet/app/src/utils/bridgeMessages";
import { sendCoins, getWalletHistory } from "./actions";

const Bridge = () => {
  console.log("Bridge loaded.");
  const [previousBalance, setPreviousBalance] = useState(null);
  // useNativeMessage hook receives message from React Native

  // wallet.named(x) is unavailable
  // Safari (has to be used for iOS debugging)
  // Unfortunately, Safari is also non standard,
  // and it does not allow access to IndexedDB in an iframe/WebView
  // Thus wallet needs to be retrieve from seed phrase every time
  useNativeMessage(async (message) => {
    console.log("Bridge received message: ", message);

    try {
      const WalletObject = message?.data?.isTestNet ? TestNetWallet : Wallet;

      switch (message.type) {
        case BRIDGE_MESSAGE_TYPES.CREATE_DEFAULT_WALLET:
          const wallet = await WalletObject.newRandom();

          console.log("Created Wallet!");
          console.log({ wallet });

          emit({
            type: RESPONSE_MESSAGE_TYPES.CREATE_DEFAULT_WALLET_RESPONSE,
            data: { wallet },
          });
          break;

        case BRIDGE_MESSAGE_TYPES.CREATE_SCRATCHPAD_WALLET:
          const walletScratchPad = await WalletObject.newRandom();

          emit({
            type: RESPONSE_MESSAGE_TYPES.CREATE_SCRATCHPAD_WALLET_RESPONSE,
            data: { wallet: walletScratchPad },
          });
          break;

        case BRIDGE_MESSAGE_TYPES.REQUEST_BALANCE_AND_ADDRESS:
          // max tracked HD address index
          const maxIndex = message?.data?.maxAddressIndex || 0;

          // for each address, get its total balance and its raw utxos
          const result = await Promise.all([...Array(maxIndex + 1).keys()].map(async (index) => {
            const wallet = await WalletObject.fromSeed(
              message?.data?.mnemonic,
              `m/44'/0'/0'/0/${index}`
            );

            const coins = (await wallet.getAddressUtxos(wallet.cashaddr)).map(val => ({...val, ...{address: wallet.cashaddr, addressIndex: index}}));
            const balance = coins.reduce((prev, curr) => prev + curr.satoshis, 0);
            return [balance, coins];
          }));
          const balances = result.map(val => val[0]);
          const coins = result.map(val => val[1]);

          // find out the latest non empty address and set it as next receiving address
          let nonZeroBalanceAddressIndex = 0;
          balances.forEach((val, index) => {
            if (val > 0) {
              nonZeroBalanceAddressIndex = index;
            }
          })
          const depositAddrIndex = nonZeroBalanceAddressIndex + 1;
          const depositWallet = await WalletObject.fromSeed(
            message?.data?.mnemonic,
            `m/44'/0'/0'/0/${depositAddrIndex}`
          );
          const depositAddress = depositWallet.cashaddr;

          // calculate total balance on all tracked addresses and compare with previously stored
          const totalBalance = balances.reduce((prev, cur) => prev + cur, 0);
          if (previousBalance === null) {
            // if previous was null, just update it without triggering toasts
          } else {
            // otherwise get delta and figure out if it was receival or detected spend
            const isReceivedCoins = parseInt(totalBalance) > parseInt(previousBalance);
            if (isReceivedCoins) {
              emit({
                type: RESPONSE_MESSAGE_TYPES.RECEIVED_COINS,
                data: {
                  name: message?.data?.name,
                  balance: totalBalance,
                },
              });
            } else {
              emit({
                type: RESPONSE_MESSAGE_TYPES.SEND_COINS_RESPONSE_DETECTED,
                data: {
                  name: message?.data?.name,
                  balance: totalBalance,
                },
              });
            }
          }
          // update the stored balance
          setPreviousBalance(totalBalance);

          // update total wallet balance and receive address in UI
          emit({
            type: RESPONSE_MESSAGE_TYPES.REQUEST_BALANCE_AND_ADDRESS_RESPONSE,
            data: {
              name: message?.data?.name,
              balance: totalBalance,
              cashaddr: depositAddress,
              maxAddressIndex: depositAddrIndex,
              coins: coins.flat(1),
            },
          });
          break;

        case BRIDGE_MESSAGE_TYPES.SEND_COINS:
          await sendCoins(WalletObject, message);
          break;

        case BRIDGE_MESSAGE_TYPES.GET_WALLET_HISTORY:
          await getWalletHistory(WalletObject, message);
          break;

        default:
          console.log("NOTE: Message type not recognised!!");
          break;
      }
    } catch (error) {
      console.log({ error });
      // Insufficient balance
      // Error is: Amount required was not met, 1971 satoshis needed, 1785 satoshis available
      if (error.toString()?.includes("Amount required was not met")) {
        const errorSegments = error?.toString()?.split(", ");
        const returnMessage = `${errorSegments?.[1]}, ${errorSegments?.[2]}.`;
        emit({
          type: RESPONSE_MESSAGE_TYPES.ERROR,
          data: {
            title: "❌ Not enough satoshis",
            text: `Once transaction fee is added. ${returnMessage}`,
          },
        });
        return;
      }

      // Generic connection errors were just annoying and often inconsequential
      // due to unreliable bridge
      // emit({
      //   type: RESPONSE_MESSAGE_TYPES.ERROR,
      //   data: {
      //     title: "❌ No connection 📶",
      //     text: "Check that your Internet is online.",
      //   },
      // });
    }
  });

  return <div style={{ height: 0 }}></div>;
};

// This statement is detected by babelTransformer as an entry point
// All dependencies are resolved, compressed and stringified into one file
export default webViewRender(<Bridge />);
