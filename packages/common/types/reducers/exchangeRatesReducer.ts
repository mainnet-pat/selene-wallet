export interface ExchangeRatesState {
  audBchPrice: string;
  btcBchPrice: string;
  cadBchPrice: string;
  cnyBchPrice: string;
  ethBchPrice: string;
  eurBchPrice: string;
  gbpBchPrice: string;
  jpyBchPrice: string;
  phpBchPrice: string;
  rubBchPrice: string;
  thbBchPrice: string;
  usdBchPrice: string;
}
