declare const TYPOGRAPHY: {
    title: {
        fontFamily: string;
        textAlign: "center";
        color: string;
        fontSize: number;
        marginBottom: number;
    };
    subtitle: {
        fontFamily: string;
        textAlign: "center";
        color: string;
        fontSize: number;
        marginBottom: number;
    };
    header: {
        fontFamily: string;
        textAlign: "center";
        color: string;
        fontSize: number;
    };
    whiteLink: {
        fontFamily: string;
        textAlign: "center";
        color: string;
        fontSize: number;
        textDecorationLine: string;
        marginTop: number;
        marginBottom: number;
    };
    h1: {
        fontFamily: string;
        textAlign: "center";
        color: string;
        fontSize: number;
        marginBottom: number;
    };
    h1black: {
        fontFamily: string;
        textAlign: "center";
        color: string;
        fontSize: number;
        marginBottom: number;
    };
    h1red: {
        fontFamily: string;
        textAlign: "center";
        color: string;
        fontSize: number;
        marginBottom: number;
    };
    h2: {
        fontFamily: string;
        textAlign: "center";
        color: string;
        fontSize: number;
        marginBottom: number;
    };
    h2Left: {
        fontFamily: string;
        textAlign: "left";
        color: string;
        fontSize: number;
        marginBottom: number;
    };
    h2black: {
        fontFamily: string;
        textAlign: "center";
        color: string;
        fontSize: number;
        marginBottom: number;
    };
    h2Green: {
        fontFamily: string;
        textAlign: "center";
        color: string;
        fontSize: number;
        marginBottom: number;
    };
    menuHeaderGreen: {
        fontFamily: string;
        textAlign: "center";
        color: string;
        fontSize: number;
    };
    subMenuHeaderWhite: {
        fontFamily: string;
        textAlign: "center";
        color: string;
        fontSize: number;
    };
    subMenuHeaderWhite14: {
        fontFamily: string;
        textAlign: "center";
        color: string;
        fontSize: number;
    };
    subMenuHeaderWhite12: {
        fontFamily: string;
        textAlign: "center";
        color: string;
        fontSize: number;
    };
    p: {
        fontFamily: string;
        textAlign: "center";
        color: string;
        fontSize: number;
        marginBottom: number;
    };
    pCentered: {
        fontFamily: string;
        textAlign: "center";
        color: string;
        fontSize: number;
    };
    pLeft: {
        fontFamily: string;
        textAlign: "left";
        color: string;
        fontSize: number;
        marginBottom: number;
    };
    pWhite: {
        fontFamily: string;
        textAlign: "center";
        color: string;
        fontSize: number;
        marginBottom: number;
    };
    pUnderlined: {
        fontFamily: string;
        textAlign: "center";
        color: string;
        fontSize: number;
        marginBottom: number;
        textDecorationLine: string;
    };
    pWhiteUnderlined: {
        fontFamily: string;
        textAlign: "center";
        color: string;
        fontSize: number;
        marginBottom: number;
        textDecorationLine: string;
    };
    pGreenUnderlined: {
        fontFamily: string;
        textAlign: "center";
        color: string;
        fontSize: number;
        marginBottom: number;
        textDecorationLine: string;
    };
    pWhiteLeft: {
        fontFamily: string;
        textAlign: "left";
        color: string;
        fontSize: number;
        marginBottom: number;
    };
    pRed: {
        fontFamily: string;
        textAlign: "center";
        color: string;
        fontSize: number;
        marginBottom: number;
    };
    spacer: {
        height: number;
    };
};
export default TYPOGRAPHY;
