"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const SPACING = {
    five: 5,
    ten: 10,
    fifteen: 15,
    twentyFive: 25,
    fifty: 50,
    borderRadius: 10,
    maxButtonHeight: 65,
};
exports.default = SPACING;
//# sourceMappingURL=spacing.js.map