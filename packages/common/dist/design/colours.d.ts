declare const COLOURS: {
    black: string;
    bchGreen: string;
    white: string;
    lightGrey: string;
    veryLightGrey: string;
    errorRed: string;
};
export default COLOURS;
