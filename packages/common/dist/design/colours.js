"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const COLOURS = {
    black: "#1C1E21",
    bchGreen: "#8DC351",
    white: "#FDFEFE",
    lightGrey: "#D4D5D8",
    veryLightGrey: "#F2F2F2",
    errorRed: "red",
};
exports.default = COLOURS;
//# sourceMappingURL=colours.js.map