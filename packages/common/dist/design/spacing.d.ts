declare const SPACING: {
    five: number;
    ten: number;
    fifteen: number;
    twentyFive: number;
    fifty: number;
    borderRadius: number;
    maxButtonHeight: number;
};
export default SPACING;
