const COLOURS = {
  black: "#1C1E21",
  bchGreen: "#8DC351",
  white: "#FDFEFE",
  lightGrey: "#D4D5D8",
  veryLightGrey: "#F2F2F2",
  errorRed: "red",
};

export default COLOURS;
